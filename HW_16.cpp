﻿#include <iostream>

int main()
{
    const int N = 4;
    int array[N][N];

    int date = 0;
    std::cout << "Enter date (number): ";
    std::cin >> date;
    int c = date % N;    

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            //Автоматически вводим данные для массива
            array[i][j] = { i + j };
            std::cout << array[i][j] << " ";
        }
        std::cout << '\n';
    }

    std::cout << "Ostatok: " << c << '\n';
    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum = sum + array[c][j];
    }
    std::cout << "Sum:" << sum << '\n';
}
